# AMLD workshop - Forecasting epileptic seizures

This repository contains the accompanying materials for the Applied Machine Learning Days 2021 (AMLD) workshop "Forecasting epileptic seizures".

The goal of the workshop is to develop models based on brain activity monitored by EEG to forecast upcoming periods of heightened seizure risk in patients with epilepsy. The workshop is based on a dataset of 18 patients, each with more than two years of continuous electroencephalographic (EEG) recordings obtained via an intracranially implanted recording device (a sort of pacemaker for the brain).

## How to run

You can run the notebooks simply by clicking [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/e-lab_amld%2Fforecasting_epileptic_seizures/HEAD) 

---

If you want to run it on you own computer:
1. Install [anaconda](https://www.anaconda.com/products/individual)
2. Clone the repository 
```bash
git clone https://gitlab.com/e-lab_amld/forecasting_epileptic_seizures.git
cd forecasting_epileptic_seizures
```
3. For the python notebooks, run 
```bash 
conda env create --file environment.yml 
conda activate AMLD
jupyter notebook
```
4. For the R notebook, run
```bash 
conda create -n ALMD-R r-essentials r-base
conda activate AMLD-R
jupyter notebook
```
and then add in the first R cell
```R
install.packages(c("ggplot2", "tscount"))
```
